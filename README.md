# Advent of Code 2019

Well, now I'm going back through the archives. *thanks* Florian.

## Implementation notes

This is not good code. It's quick and dirty problem solving based on what was in my head when I wrote it. Maybe I felt like using `Stream`s, maybe I felt like indexing arrays in `for` loops. 

`main` throws `Exception`.

## Day notes
### [Day 1](Day1.java)
I'm starting to notice that if I pay attention to the bolded text in the problem description, I get some implementation hints. In this one, `Module` becomes a class with field `mass`. Add a method to calculate the fuel required, and we have some nice object oriented programming going on. I read the input file twice because I'm lazy.

### [Day 2](Day2.java)
Oh cool, we're building a simple computer! For my cs3-something-something class we did a bunch of labs like this. Bitwise cleverness and implementing `malloc` in a synthetic (simplified) operating system. It was probably my favorite CS class of all time. This also made me look up 0x10c again. 

### [Day 3](Day3.java)
The fact that we're moving on a grid helps a lot when it comes to checking for intersections. I also assume that none of the wires overlap while colinear, because that would make the premise of the problem much harder to deal with.

### [Day 4](Day4.java)
For the filter conditions, it generally helped to write out the logic as if/else statements, then recognize the structure and collapse it into a truth table.
