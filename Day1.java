import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Day1 {
    public static void main(String... args) throws Exception{
        Path input = Paths.get("./Day1.input");

        // part 1 - calculate total fuel required for all modules
        int totalFuel = Files.readAllLines(input).stream()
            .mapToInt(Integer::parseInt)
            .map(mass -> mass / 3 - 2)
            .sum();

        System.out.printf("Part 1 fuel required: %d%n", totalFuel);


        // part 2 - correct the fuel equation to include fuel for the fuel
        int correctFuel = Files.readAllLines(input).stream()
            .mapToInt(Integer::parseInt)
            .mapToObj(Module::new)
            .mapToInt(Module::fuelRequired)
            .sum();

        System.out.printf("Part 2 fuel required: %d%n", correctFuel);

    }

    private static class Module{
        int mass;

        Module(int mass){
            this.mass = mass;
        }

        int fuelRequired() {
            int fuelRequired = mass / 3 - 2;
            int newFuel = fuelRequired / 3 - 2;
            while(newFuel > 0){
                fuelRequired += newFuel;
                newFuel = newFuel / 3 - 2;
            }

            return fuelRequired;
        }
    }


}