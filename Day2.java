import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class Day2 {

    public static void main(String... args) throws Exception {
        // part 1 - load up the computer and run until halt. output the value at memory 0
        Path input = Paths.get("./Day2.input");
        String intcode = Files.readString(input);
        Computer computer = new Computer(intcode);
        
        System.out.printf("Part 1 - value at memory address 0: %d%n", computer.run(12, 2));

        // part 2 - find the pair of inputs that results in an ouput of 19690720
        int noun = 0;
        int verb = 0;
        search:
        for(; noun < 99; noun++){
            for(verb = 0; verb < 99; verb++){
                if (computer.run(noun, verb) == 19690720) break search;
            }
        }

        System.out.printf("Part 2 - 100 * noun + verb = %d%n", 100 * noun + verb);
        
    }

    private static class Computer {
        int[] rom;
        int[] memory;

        Computer(String intcode){
            rom = Arrays.stream(intcode.trim().replaceAll("\\s+","").split(",")).mapToInt(Integer::parseInt).toArray();
        }

        int run(int noun, int verb){
            // reset memory
            memory = rom.clone();
            memory[1] = noun;
            memory[2] = verb;

            // initialize the instruction pointer
            int inst = 0;
            int opcode = memory[inst];

            // run
            while(opcode != 99){
                // instruction set
                switch (opcode){
                    case 1:
                        memory[memory[inst + 3]] = memory[memory[inst+1]] + memory[memory[inst+2]];
                        inst += 4;
                        break;
                    case 2:
                        memory[memory[inst + 3]] = memory[memory[inst+1]] * memory[memory[inst+2]];
                        inst += 4;
                        break;
                }

                // load the next opcode
                opcode = memory[inst];
            }

            return memory[0];
        }
    }
    
}
