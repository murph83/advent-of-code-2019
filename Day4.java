import java.util.Arrays;
import java.util.function.IntPredicate;
import java.util.stream.IntStream;

/**
 * Day4
 */
public class Day4 {

    public static void main(String... args) throws Exception{
        //inputs
        int min = 197487;
        int max = 673251;

        long valid = IntStream.rangeClosed(min, max)
            .filter(monotonic)
            .filter(hasTwoConsecutiveMatchingDigits)
            .count();

        System.out.printf("Part 1 - number of passwords matching criteria: %d%n", valid);

        long moreValid = IntStream.rangeClosed(min, max)
            .filter(monotonic)
            .filter(hasNoMoreThanTwoConsecutiveMatchingDigits)
            .count();

        System.out.printf("Part 2 - number of passwords matching criteria: %d%n", moreValid);
    }

    private static IntPredicate monotonic = (i) -> {
        int[] digits = new int[]{i/100000%10, i/10000%10, i/1000%10, i/100%10, i/10%10, i%10};
        return digits[0] <= digits[1] && digits[1] <= digits[2] && digits[2] <= digits[3] && digits[3] <= digits[4] && digits[4] <= digits[5];
    };

    private static IntPredicate hasTwoConsecutiveMatchingDigits = (i) -> {
        int[] digits = new int[]{i/100000%10, i/10000%10, i/1000%10, i/100%10, i/10%10, i%10};
        return digits[0] == digits[1] || digits[1] == digits[2] || digits[2] == digits[3] || digits[3] == digits[4] || digits[4] == digits[5];
    };

    private static IntPredicate hasNoMoreThanTwoConsecutiveMatchingDigits = (i) -> {
        int[] digits = new int[]{i/100000%10, i/10000%10, i/1000%10, i/100%10, i/10%10, i%10};
        // since matching digits are guaranteed to be next to each other by the monotonic filter, we just need to check that there is a digit with cardinality exactly 2
        int[] frequency = new int[10];
        for(int d : digits){
            frequency[d]++;
        }
        return Arrays.stream(frequency).anyMatch(f -> f == 2);
    };


}