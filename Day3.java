import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Day3 {

    public static void main(String... args) throws Exception {
        Path input = Paths.get("./Day3.input");
        List<String> wires = Files.readAllLines(input);

        List<Line> redWire = wireNodes(wires.get(0));
        List<Line> blueWire = wireNodes(wires.get(1));

        List<Node> intersections = new ArrayList<>();
        for(Line red : redWire){
            for(Line blue : blueWire){
                if (red.intersects(blue)) {
                    intersections.add(red.intersection(blue));
                }
            }
        }

        int distance = intersections.stream()
            .filter(n -> !new Node(0,0).equals(n))
            .mapToInt(Node::manhattanDistance)
            .min().getAsInt();

        System.out.printf("Part 1 - Manhattan distance of nearest crossing: %d%n", distance);

        int latency = intersections.stream()
            .filter(n -> !new Node(0,0).equals(n))
            .mapToInt(n -> latencyToNode(blueWire, n) + latencyToNode(redWire, n))
            .min().getAsInt();

        System.out.printf("Part 1 - total latency of nearest crossing: %d%n", latency);


    }

    private static int latencyToNode(List<Line> wire, Node node){
        int latency = 0;
        for(Line line : wire){
            if (node.x == line.origin.x && node.x == line.target.x && node.y > Math.min(line.origin.y, line.target.y) && node.y < Math.max(line.origin.y, line.target.y))
            {
                // node on vertical line
                latency += Math.abs(node.y - line.origin.y);
                break;
            } 
            else if (node.y == line.origin.y && node.y == line.target.y && node.x > Math.min(line.origin.x, line.target.x) && node.x < Math.max(line.origin.x, line.target.x))
            {
                // node on horizontal line
                latency += Math.abs(node.x - line.origin.x);
                break;
            } 
            else 
            {
                // node not on line
                latency += Math.abs(line.target.x - line.origin.x) + Math.abs(line.target.y - line.origin.y);
            }
        }

        return latency;
    }

    private static List<Line> wireNodes(String wire) {
        List<Line> lines = new ArrayList<>();
        int x = 0;
        int y = 0;
        for (String move : wire.split(",")) {
            Node origin = new Node(x, y);
            String direction = move.substring(0, 1);
            int distance = Integer.parseInt(move.substring(1));
            switch (direction) {
                case "U":
                    y += distance;
                    break;
                case "D":
                    y -= distance;
                    break;
                case "R":
                    x += distance;
                    break;
                case "L":
                    x -= distance;
                    break;
            }
            Node target = new Node(x, y);
            lines.add(new Line(origin, target));
        }

        return lines;
    }

    private static class Node {
        int x;
        int y;

        Node(int x, int y) {
            this.x = x;
            this.y = y;
        }

        int manhattanDistance(){
            return Math.abs(x) + Math.abs(y);
        }

        public boolean equals(Object obj) {
            Node other = (Node) obj;
            return other.x == x && other.y == y;
        }

        public int hashCode() {
            int hash = 17;
            hash = 31 * hash + Integer.hashCode(x);
            hash = 31 * hash + Integer.hashCode(y);
            return hash;
        }

        public String toString() {
            return Arrays.toString(new int[] { x, y });
        }
    }

    private static class Line {
        Node origin;
        Node target;

        Line(Node origin, Node target) {
            this.origin = origin;
            this.target = target;
        }

        public String toString() {
            return origin.toString() + " -> " + target.toString();
        }

        // assume lines are at right angles
        Node intersection(Line other){
            if(origin.x == target.x){
                return new Node(origin.x, other.origin.y);
            } else {
                return new Node(other.origin.x, origin.y);
            }

        }

        // assume lines are at right angles
        boolean intersects(Line other) {

            int verticalx;
            int maxx;
            int minx;

            int horizontaly;
            int maxy;
            int miny;

            if(origin.x == target.x){
                verticalx = origin.x;
                minx = Math.min(other.origin.x, other.target.x);
                maxx = Math.max(other.origin.x, other.target.x);

                horizontaly = other.origin.y;
                miny = Math.min(origin.y, target.y);
                maxy = Math.max(origin.y, target.y);
            } else {
                verticalx = other.origin.x;
                minx = Math.min(origin.x, target.x);
                maxx = Math.max(origin.x, target.x);

                horizontaly = origin.y;
                miny = Math.min(other.origin.y, other.target.y);
                maxy = Math.max(other.origin.y, other.target.y);
            }

            return (verticalx > minx && verticalx < maxx) && (horizontaly > miny && horizontaly < maxy);
        
        }
        
    }

}
